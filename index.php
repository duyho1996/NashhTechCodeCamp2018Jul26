<?php
require 'vendor/autoload.php';

function __autoload($class)
{
	$parts = explode('\\', $class);
	require_once 'src\\csv\\' . end($parts) . '.php';
}

use csv\CSVParserImp;
use csv\CSVParserLeague;
use csv\CSVFormatterImp;

const alignMap = [
	'left' => CSVFormatterImp::ALIGN_LEFT,
	'right' => CSVFormatterImp::ALIGN_RIGHT,
];


function parseCommands($argv) {
	$options = $argv;
	unset($options[0]);

	$res = [];
	foreach ($options as $option) {
		$commandMap = explode("=", $option);
		$res[$commandMap[0]] = @$commandMap[1];
	}

	return $res;
}

$options = parseCommands($argv);
if (empty($options['src'])) {
	die ('Source not found');
}

$source = file_get_contents($options['src']);
$hasHeader = isset($options['has-header']) ? $options['has-header'] : 1;
$sortByColumn = isset($options['sort-column']) ? $options['sort-column']: -1;
$sort = isset($options['sort'])? $options['sort'] : SORT_ASC;
$currencyColumn = isset($options['currency-column'])? $options['currency-column'] : -1;
$align = isset($options['align'])? $options['align'] : 'left';
$align = alignMap[$align];

$parser = new \csv\CSVParserImp($source, $hasHeader);

$formatter = new CSVFormatterImp($parser);
if ($sortByColumn > 0 && $sort) {
	$formatter->sort( $sortByColumn, $sort );
}
if ($currencyColumn > 1) {
	$formatter->formatCurrency( $currencyColumn );
	$formatter->alignColumn($currencyColumn, CSVFormatterImp::ALIGN_RIGHT);
}
if ($align > 0) {
	$formatter->align( $align );
}
if ($currencyColumn > 1) {
	$formatter->alignColumn($currencyColumn, CSVFormatterImp::ALIGN_RIGHT);
}
echo $formatter->toString();
