<?php
/**
 * Created by PhpStorm.
 * User: duyhodn
 * Date: 7/26/2018
 * Time: 10:44 AM
 */
namespace csv;
interface CSVParser {
	public function getCsv(): array;
}