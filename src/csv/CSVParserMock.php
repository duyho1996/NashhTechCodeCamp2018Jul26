<?php
/**
 * Created by PhpStorm.
 * User: duyhodn
 * Date: 7/26/2018
 * Time: 1:19 PM
 */

namespace csv;
class CSVParserMock implements CSVParser {
	public function getCsv(): array {
		return [
			'headers' => [ 'Last', 'First', 'Salary', '' ],
			'data'    => [
				[ 'Ling', 'Mai', '55900', '' ],
				[ 'Johnson', 'Jim', '56500', '' ],
				[ 'Jones', 'Aaron', '46000', '' ],
			]
		];
	}
}