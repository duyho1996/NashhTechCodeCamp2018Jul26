<?php
/**
 * Created by PhpStorm.
 * User: duyhodn
 * Date: 7/26/2018
 * Time: 10:44 AM
 */

namespace csv;
class CSVParserImp implements CSVParser {

	private $csv    = [];
	private $source = "";

	private $headers   = [];
	private $hasHeader = false;

	/**
	 * CSVParserImp constructor.
	 */
	public function __construct( $source, $hasHeader = false ) {
		$this->hasHeader = $hasHeader;
		$this->source    = $source;
		$this->parse( $source );
	}

	public function parse( $string = "" ) {
		if ( empty( $string ) ) {
			$string = $this->source;
		}

		$rows = explode( "\r\n", $string );

		$result = [];
		foreach ( $rows as $row ) {
			$columns = preg_split( '/(?:"[^"]*"|)\K\s*(,\s*|$)/', $row );
			if ( empty( $columns ) ) {
				continue;
			}

			foreach ( $columns as $key => $column ) {
				if ( ! empty( $column ) && $column[0] == '"' && $column[ strlen( $column ) - 1 ] == '"' ) {
					$columns[ $key ] = substr( $column, 1, - 1 );
				}
			}
			$result[] = $columns;
		}

		if ( $this->hasHeader ) {
			$this->headers = $result[0];
			unset( $result[0] );
		}
		$this->csv = array_values( $result );
	}

	/**
	 * @return array
	 */
	public function getCsv(): array {
		return [
			'headers' => $this->headers,
			'data'    => $this->csv
		];
	}

	/**
	 * @param array $headers
	 */
	public function setHeaders( array $headers ) {
		$this->hasHeader = true;
		$this->headers   = $headers;
	}

	/**
	 * @param string $source
	 */
	public function setSource( string $source ) {
		$this->source = $source;
	}
}