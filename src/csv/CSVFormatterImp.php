<?php
/**
 * Created by PhpStorm.
 * User: duyhodn
 * Date: 7/26/2018
 * Time: 10:43 AM
 */

namespace csv;
class CSVFormatterImp implements CSVFormatter {
	const ALIGN_LEFT  = 1;
	const ALIGN_RIGHT = 2;

	private $headers = [];
	private $data    = [];

	public function __construct( CSVParser $parser ) {
		$csv = $parser->getCsv();
		if ( ! empty( $csv['headers'] ) ) {
			$this->headers = $csv['headers'];
		}
		$this->data = $csv['data'];
	}

	/**
	 * @param array $headers
	 */
	public function setHeaders( array $headers ) {
		$this->headers = $headers;
	}

	public function toString(): string {
		$headerString = implode( " ", $this->headers ) . "\r\n";

		$rows         = [];
		$maxRowLength = strlen( $headerString );
		foreach ( $this->data as $row ) {
			$rowString = implode( " ", $row );
			if ( ! empty( $rowString ) ) {
				$maxRowLength = max( $rowString, $maxRowLength );
				$rows[]       = $rowString;
			}
		}
		$separatorString = str_repeat( "-", $maxRowLength ) . "\r\n";
		$bodyString      = implode( "\r\n", $rows );

		if (empty($this->headers))
			return $bodyString;

		return $headerString . $separatorString . $bodyString;
	}

	private function getLargestLengthsByColumns() {
		$largestLengths = [];
		$columnNum      = 0;
		while ( true ) {
			$largestLength = - 1;
			foreach ( $this->data as $rows ) {
				if ( isset( $rows[ $columnNum ] ) ) {
					$largestLength = max( $largestLength, strlen( $rows[ $columnNum ] ) );
				}
			}
			if ( $largestLength == - 1 ) {
				break;
			}
			$largestLengths[] = $largestLength;
			$columnNum ++;
		}

		return $largestLengths;
	}

	private function getMaxColumnNum() {
		$result = 0;
		foreach ( $this->data as $columns ) {
			$result = max( $result, count( $columns ) );
		}

		return $result;
	}

	public function alignColumn( $columnNum, $align = self::ALIGN_LEFT ) {
		$largestLengths = $this->getLargestLengthsByColumns();
		$data           = array_merge( [ $this->headers ], $this->data );
		foreach ( $data as $rowNum => $rows ) {
			if ( empty( $data[ $rowNum ][ $columnNum ] ) ) {
				continue;
			}
			if (empty($largestLengths[$columnNum]))
				continue;

			$value = trim( $data[ $rowNum ][ $columnNum ], " " );
			$paddingSpaces = str_repeat( " ", max( 0, $largestLengths[ $columnNum ] - strlen( $value ) ) );
			if ( $align == self::ALIGN_LEFT ) {
				$value .= $paddingSpaces;
			} else if ( $align == self::ALIGN_RIGHT ) {
				$value = $paddingSpaces . $value;
			} else {
				// do nothing
			}
			$data[ $rowNum ][ $columnNum ] = $value;
		}
		if ( ! empty( $this->headers ) ) {
			$this->headers = $data[0];
			unset( $data[0] );
		}

		$this->data = array_values( $data );
	}

	public function align( $align = self::ALIGN_LEFT ) {
		$maxColumnNum = $this->getMaxColumnNum();
		for ( $i = 0; $i < $maxColumnNum; $i ++ ) {
			$this->alignColumn( $i, $align );
		}
	}

	public function formatCurrency( $columnNum ) {
		foreach ( $this->data as $rowNum => $columns ) {
			if ( empty( $columns[ $columnNum ] ) || ! is_numeric( $columns[ $columnNum ] ) ) {
				continue;
			}

			$formattedValue                      = "$" . number_format( $columns[ $columnNum ], 2 );
			$this->data[ $rowNum ][ $columnNum ] = $formattedValue;
		}
	}

	/**
	 * @param $columnNum
	 * @param int $order
	 */
	public function sort( $columnNum, $order = SORT_ASC ) {
		$data = $this->data;
		usort( $data, function ( $a, $b ) use ( $order, $columnNum ) {
			if ( empty( $a[ $columnNum ] ) || empty( $b[ $columnNum ] ) ) {
				return 0;
			}
			if ( is_numeric( $a[ $columnNum ] ) || is_numeric( $b[ $columnNum ] ) ) {
				$aVal = (float) $a[ $columnNum ];
				$bVal = (float) $b[ $columnNum ];
			} else {
				$aVal = $a[ $columnNum ];
				$bVal = $b[ $columnNum ];
			}

			if ( $aVal == $bVal ) {
				return 0;
			}

			if ( $order == SORT_ASC ) {
				return ( $aVal < $bVal ) ? - 1 : 1;
			}
			if ( $order == SORT_DESC ) {
				return ( $aVal > $bVal ) ? - 1 : 1;
			}
		} );
		$this->data = $data;
	}


	/**
	 * @return array
	 */
	public function getHeaders(): array {
		return $this->headers;
	}

	/**
	 * @return array
	 */
	public function getData(): array {
		return $this->data;
	}

}