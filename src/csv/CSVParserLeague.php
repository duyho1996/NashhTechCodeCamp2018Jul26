<?php
/**
 * Created by PhpStorm.
 * User: duyhodn
 * Date: 7/26/2018
 * Time: 3:01 PM
 */

namespace csv;


use League\Csv\Exception;
use League\Csv\Reader;

class CSVParserLeague implements CSVParser {

	private $csv    = [];
	private $source = "";

	private $headers   = [];
	private $hasHeader = false;

	/**
	 * CSVParserImp constructor.
	 */
	public function __construct( $source, $hasHeader = false ) {
		$this->hasHeader = $hasHeader;
		$this->source    = $source;
		$this->parse( $source );
	}

	public function parse( $string = "" ) {
		if ( empty( $string ) ) {
			$string = $this->source;
		}

		$csv = Reader::createFromString($string);
		if ($this->hasHeader) {
			try {
				$csv->setHeaderOffset( 0 );
			} catch ( Exception $e ) {
			}
		}

		$this->headers = $csv->getHeader();

		$data = [];
		$recordsIterator = $csv->getRecords();
		$recordsIterator->next();
		while ($recordsIterator->valid()) {
			$data[] = array_values($recordsIterator->current());
			$recordsIterator->next();
		}
		$this->csv = $data;
	}

	/**
	 * @return array
	 */
	public function getCsv(): array {
		return [
			'headers' => $this->headers,
			'data'    => $this->csv
		];
	}
}