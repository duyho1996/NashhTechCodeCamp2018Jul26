<?php
/**
 * Created by PhpStorm.
 * User: duyhodn
 * Date: 7/26/2018
 * Time: 10:43 AM
 */

namespace csv;
interface CSVFormatter {
	public function align();

	public function formatCurrency( $columnNum );

	public function sort( $columnNum, $order = SORT_ASC );

	public function toString(): string;
}