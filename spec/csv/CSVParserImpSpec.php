<?php

namespace spec\csv;

use csv\CSVParserImp;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CSVParserImpSpec extends ObjectBehavior {
	private $sourceString           = "Ling,Mai,55900\r\nJohnson,Jim,56500\r\nJones,Aaron,46000";
	private $sourceStringWithHeader = "Last,First,Salary\r\nLing,Mai,55900\r\nJohnson,Jim,56500\r\nJones,Aaron,46000";

	function it_can_parse_csv_string() {
		$expectedCsv = [
			'headers' => [],
			'data'    => [
				[ 'Ling', 'Mai', '55900', '' ],
				[ 'Johnson', 'Jim', '56500', '' ],
				[ 'Jones', 'Aaron', '46000', '' ],
			]
		];
		$this->beConstructedWith( $this->sourceString );
		$this->getCsv()->shouldReturn( $expectedCsv );
	}

	function it_can_parse_csv_string_with_header() {
		$expectedCsv = [
			'headers' => [ 'Last', 'First', 'Salary', '' ],
			'data'    => [
				[ 'Ling', 'Mai', '55900', '' ],
				[ 'Johnson', 'Jim', '56500', '' ],
				[ 'Jones', 'Aaron', '46000', '' ],
			]
		];
		$this->beConstructedWith( $this->sourceStringWithHeader, true );
		$this->getCsv()->shouldReturn( $expectedCsv );
	}

	function it_can_parse_empty_csv() {
		$expectedCsv = [
			'headers' => [],
			'data'    => [["", ""]]
		];
		$this->beConstructedWith( "");
		$this->getCsv()->shouldReturn( $expectedCsv );
	}
}
