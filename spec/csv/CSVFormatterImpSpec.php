<?php

namespace spec\csv;

use csv\CSVParserMock;
use PhpSpec\ObjectBehavior;
use ReflectionProperty;

class CSVFormatterImpSpec extends ObjectBehavior {
	function it_can_align_left() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ 'Ling   ', 'Mai  ', '55900', '' ],
			[ 'Johnson', 'Jim  ', '56500', '' ],
			[ 'Jones  ', 'Aaron', '46000', '' ],
		];
		$this->align();
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_align_right() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ '   Ling', '  Mai', '55900', '' ],
			[ 'Johnson', '  Jim', '56500', '' ],
			[ '  Jones', 'Aaron', '46000', '' ],
		];
		$this->align( 2 );
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_format_currency() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ 'Ling', 'Mai', '$55,900.00', '' ],
			[ 'Johnson', 'Jim', '$56,500.00', '' ],
			[ 'Jones', 'Aaron', '$46,000.00', '' ],
		];
		$this->formatCurrency( 2 );
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_skip_format_currency_for_non_number_value() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ 'Ling', 'Mai', '55900', '' ],
			[ 'Johnson', 'Jim', '56500', '' ],
			[ 'Jones', 'Aaron', '46000', '' ],
		];

		$this->formatCurrency( 1 );
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_sort_number_asc_by_column() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ 'Jones', 'Aaron', '46000', '' ],
			[ 'Ling', 'Mai', '55900', '' ],
			[ 'Johnson', 'Jim', '56500', '' ],
		];
		$this->sort( 2 );
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_sort_number_desc_by_column() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ 'Johnson', 'Jim', '56500', '' ],
			[ 'Ling', 'Mai', '55900', '' ],
			[ 'Jones', 'Aaron', '46000', '' ],
		];
		$this->sort( 2, SORT_DESC );
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_sort_string_asc_by_column() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ 'Johnson', 'Jim', '56500', '' ],
			[ 'Jones', 'Aaron', '46000', '' ],
			[ 'Ling', 'Mai', '55900', '' ],
		];
		$this->sort( 0 );
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_sort_string_desc_by_column() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = [
			[ 'Ling', 'Mai', '55900', '' ],
			[ 'Jones', 'Aaron', '46000', '' ],
			[ 'Johnson', 'Jim', '56500', '' ],
		];
		$this->sort( 0, SORT_DESC );
		$this->getData()->shouldReturn( $expectedResult );
	}

	function it_can_render_csv_to_string() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );

		$expectedResult = "Last    First     Salary \r\n---------------------------\r\nJohnson Jim   $56,500.00 \r\nLing    Mai   $55,900.00 \r\nJones   Aaron $46,000.00 ";
		$this->sort( 2, SORT_DESC );
		$this->formatCurrency( 2 );
		$this->align();
		$this->alignColumn( 2, 2 );
		$this->sort( 2, SORT_DESC );
		$this->toString()->shouldReturn( $expectedResult );
	}

	function it_can_get_set_headers() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );
		$headers = ['header1', 'header2'];

		$this->setHeaders($headers);
		$this->getHeaders()->shouldReturn($headers);
	}

	function it_can_get_data() {
		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );
		$expectedResult = [
			[ 'Ling', 'Mai', '55900', '' ],
			[ 'Johnson', 'Jim', '56500', '' ],
			[ 'Jones', 'Aaron', '46000', '' ],
		];

		$this->getData()->shouldReturn($expectedResult);
	}

	function it_can_align_column_to_left() {

		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );
		$this->alignColumn(1, $this->getWrappedObject()::ALIGN_LEFT);
		$expectedResult = [
			[ 'Ling', 'Mai  ', '55900', '' ],
			[ 'Johnson', 'Jim  ', '56500', '' ],
			[ 'Jones', 'Aaron', '46000', '' ],
		];

		$this->getData()->shouldReturn($expectedResult);
	}

	function it_can_align_column_to_right() {

		$parser = new CSVParserMock();
		$this->beConstructedWith( $parser );
		$this->alignColumn(1, $this->getWrappedObject()::ALIGN_RIGHT);
		$expectedResult = [
			[ 'Ling', '  Mai', '55900', '' ],
			[ 'Johnson', '  Jim', '56500', '' ],
			[ 'Jones', 'Aaron', '46000', '' ],
		];

		$this->getData()->shouldReturn($expectedResult);
	}
}
